%define _build_id_links none

%ifarch x86_64
%define karch x86_64
%define karch32 x86
%endif

# whether to use LLVM-built kernel package dependencies
%define llvm_kbuild 0

Name: kernel-xanmod-lts
Summary: The Linux Kernel with XanMod Patches

%define _basekver 5.10
%define _stablekver 82
Version: %{_basekver}.%{_stablekver}

%define flaver xm1

Release:%{flaver}.0%{?dist}

%define rpmver %{version}-%{release}
%define krelstr %{release}.%{_arch}
%define kverstr %{version}-%{krelstr}

License: GPLv2 and Redistributable, no modifications permitted
Group: System Environment/Kernel
Vendor: The Linux Community and XanMod maintainer(s)
URL: https://xanmod.org
Source0: kernel-%{version}.tar.gz
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}
Source2: rnread-%{name}
BuildRequires: python3-devel make perl-generators perl-interpreter openssl-devel bison flex findutils git-core perl-devel openssl elfutils-devel gawk binutils m4 tar hostname bzip2 bash gzip xz bc diffutils redhat-rpm-config net-tools elfutils patch rpm-build dwarves kmod perl-Carp rsync grubby
%if %{llvm_kbuild}
BuildRequires: llvm%{_isa} lld%{_isa} clang%{_isa}
%else
BuildRequires: gcc%{_isa}
%endif
%if 0%{?rhel} == 7
BuildRequires: centos-release-scl devtoolset-8-gcc-c++ devtoolset-8-gcc epel-release zstd
%else
BuildRequires: libkcapi-hmaccalc
%endif
Requires: %{name}-core-%{rpmver} = %{kverstr}, %{name}-modules-%{rpmver} = %{kverstr}
Provides: rnread-%{name} = 1
Provides: %{name}%{_basekver} = %{rpmver}

%description
The kernel-%{flaver} meta package

%package core
Summary: Kernel core package
Group: System Environment/Kernel
Provides: installonlypkg(kernel), kernel = %{rpmver}, kernel-core = %{rpmver}, kernel-core-uname-r = %{kverstr}, kernel-uname-r = %{kverstr}, kernel-%{_arch} = %{rpmver}, kernel-core%{_isa} = %{rpmver}, kernel-core-%{rpmver} = %{kverstr}, %{name}-core-%{rpmver} = %{kverstr}, kernel-drm-nouveau = 16
Provides: %{name}%{_basekver}-core = %{rpmver}
Requires: bash, coreutils, dracut, linux-firmware, /usr/bin/kernel-install, kernel-modules-%{rpmver} = %{kverstr}
%if 0%{?rhel} != 7
Supplements: %{name} = %{rpmver}
%endif
%description core
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system: memory allocation, process allocation, device
input and output, etc.

%package modules
Summary: Kernel modules to match the core kernel
Group: System Environment/Kernel
Provides: installonlypkg(kernel-module), kernel-modules = %{rpmver}, kernel-modules%{_isa} = %{rpmver}, kernel-modules-uname-r = %{kverstr}, kernel-modules-%{_arch} = %{rpmver}, kernel-modules-%{rpmver} = %{kverstr}, %{name}-modules-%{rpmver} = %{kverstr}
Provides: %{name}%{_basekver}-modules = %{rpmver}
%if 0%{?rhel} != 7
Supplements: %{name} = %{rpmver}
%endif
%description modules
This package provides kernel modules for the core kernel package.

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Provides: kernel-headers = %{kverstr}, glibc-kernheaders = 3.0-46, kernel-headers%{_isa} = %{kverstr}
Obsoletes: kernel-headers < %{kverstr}, glibc-kernheaders < 3.0-46
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Development package for building kernel modules to match the %{kverstr} kernel
Group: System Environment/Kernel
AutoReqProv: no
Requires: findutils perl-interpreter openssl-devel flex make bison elfutils-libelf-devel
%if %{llvm_kbuild}
Requires: llvm%{_isa} lld%{_isa} clang%{_isa}
%else
Requires: gcc%{_isa}
%endif
%if 0%{?rhel} != 7
Enhances: dkms akmods
%endif
Provides: installonlypkg(kernel), kernel-devel = %{rpmver}, kernel-devel-uname-r = %{kverstr}, kernel-devel-%{_arch} = %{rpmver}, kernel-devel%{_isa} = %{rpmver}, kernel-devel-%{rpmver} = %{kverstr}, %{name}-devel-%{rpmver} = %{kverstr}
Provides: %{name}%{_basekver}-devel = %{rpmver}
%description -n %{name}-devel
This package provides kernel headers and makefiles sufficient to build modules
against the %{kverstr} kernel package.

%prep
%setup -q -n kernel-%{version}

%build
gcc --version
%if 0%{?rhel} == 7
echo "Switching to gcc-8.3.1"
export PATH="/opt/rh/devtoolset-8/root/usr/bin:$PATH"
gcc --version
%endif
make %{?_smp_mflags} EXTRAVERSION=-%{krelstr}
gcc ./scripts/sign-file.c -o ./scripts/sign-file -lssl -lcrypto

%install

ImageName=$(make image_name | tail -n 1)

mkdir -p %{buildroot}/boot

cp -v $ImageName %{buildroot}/boot/vmlinuz-%{kverstr}
chmod 755 %{buildroot}/boot/vmlinuz-%{kverstr}

make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot} modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install

cp -v System.map %{buildroot}/boot/System.map-%{kverstr}
cp -v .config %{buildroot}/boot/config-%{kverstr}
bzip2 -9 --keep vmlinux
mv vmlinux.bz2 %{buildroot}/boot/vmlinux-%{kverstr}

# prepare -devel files
### most of the things here are derived from the Fedora kernel.spec
### see 
##### https://src.fedoraproject.org/rpms/kernel/blob/rawhide/f/kernel.spec#_1613
### for details
rm -f %{buildroot}/lib/modules/%{kverstr}/build
rm -f %{buildroot}/lib/modules/%{kverstr}/source
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build
mkdir -p %{buildroot}/lib/modules/%{kverstr}/extra
mkdir -p %{buildroot}/lib/modules/%{kverstr}/internal
mkdir -p %{buildroot}/lib/modules/%{kverstr}/updates
mkdir -p %{buildroot}/lib/modules/%{kverstr}/weak-updates
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/include/config/
 # Remove testing headers
find . -name "*.h.s" -delete
cp -v --parents $(find -type f -name "Makefile*" -o -name "Kconfig*") %{buildroot}/lib/modules/%{kverstr}/build
if [ ! -e Module.symvers ] ; then
	touch Module.symvers
fi
cp -v Module.symvers %{buildroot}/lib/modules/%{kverstr}/build
cp -v System.map %{buildroot}/lib/modules/%{kverstr}/build
if [ -s Module.markers ] ; then
	cp -v Module.markers %{buildroot}/lib/modules/%{kverstr}/build
fi
gzip -c9 < Module.symvers > %{buildroot}/boot/symvers-%{kverstr}.gz
cp -v %{buildroot}/boot/symvers-%{kverstr}.gz %{buildroot}/lib/modules/%{kverstr}/symvers.gz

rm -rf %{buildroot}/lib/modules/%{kverstr}/build/scripts
rm -rf %{buildroot}/lib/modules/%{kverstr}/build/include
cp -v .config %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a scripts %{buildroot}/lib/modules/%{kverstr}/build
rm -rf %{buildroot}/lib/modules/%{kverstr}/build/scripts/tracing
rm -f %{buildroot}/lib/modules/%{kverstr}/build/scripts/spdxcheck.py

	
    # Files for 'make scripts' to succeed with kernel-devel.
    mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/security/selinux/include
    cp -v -a --parents security/selinux/include/classmap.h %{buildroot}/lib/modules/%{kverstr}/build
    cp -v -a --parents security/selinux/include/initial_sid_to_string.h %{buildroot}/lib/modules/%{kverstr}/build
    mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/tools/include/tools
    cp -v -a --parents tools/include/tools/be_byteshift.h %{buildroot}/lib/modules/%{kverstr}/build
    cp -v -a --parents tools/include/tools/le_byteshift.h %{buildroot}/lib/modules/%{kverstr}/build
 
    if [ -f tools/objtool/objtool ]; then
      cp -v -a tools/objtool/objtool %{buildroot}/lib/modules/%{kverstr}/build/tools/objtool/ || :
    fi
    if [ -d arch/%{karch32}/scripts ]; then
      cp -v -a arch/%{karch32}/scripts %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch} || :
    fi
    if [ -f arch/%{karch32}/*lds ]; then
      cp -v -a arch/%{karch32}/*lds %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch}/ || :
    fi
    if [ -f arch/%{karch32}/kernel/module.lds ]; then
      cp -v -a --parents arch/%{karch32}/kernel/module.lds %{buildroot}/lib/modules/%{kverstr}/build/
    fi
    rm -f %{buildroot}/lib/modules/%{kverstr}/build/scripts/*.o
    rm -f %{buildroot}/lib/modules/%{kverstr}/build/scripts/*/*.o
%ifarch ppc64le
    cp -v -a --parents arch/powerpc/lib/crtsavres.[So] %{buildroot}/lib/modules/%{kverstr}/build/
%endif
    if [ -d arch/%{karch}/include ]; then
      cp -v -a --parents arch/%{karch}/include %{buildroot}/lib/modules/%{kverstr}/build/
    fi
%ifarch aarch64
    # arch/arm64/include/asm/xen references arch/arm
    cp -v -a --parents arch/arm/include/asm/xen %{buildroot}/lib/modules/%{kverstr}/build/
    # arch/arm64/include/asm/opcodes.h references arch/arm
    cp -v -a --parents arch/arm/include/asm/opcodes.h %{buildroot}/lib/modules/%{kverstr}/build/
%endif
    # include the machine specific headers for ARM variants, if available.
%ifarch %{arm}
    if [ -d arch/%{karch}/mach-${Flavour}/include ]; then
      cp -v -a --parents arch/%{karch}/mach-${Flavour}/include %{buildroot}/lib/modules/%{kverstr}/build/
    fi
    # include a few files for 'make prepare'
    cp -v -a --parents arch/arm/tools/gen-mach-types %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/arm/tools/mach-types %{buildroot}/lib/modules/%{kverstr}/build/
 
%endif
    cp -v -a include %{buildroot}/lib/modules/%{kverstr}/build/include
%ifarch i686 x86_64
    # files for 'make prepare' to succeed with kernel-devel
    cp -v -a --parents arch/x86/entry/syscalls/syscall_32.tbl %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/entry/syscalls/syscalltbl.sh %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/entry/syscalls/syscallhdr.sh %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/entry/syscalls/syscall_64.tbl %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/tools/relocs_32.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/tools/relocs_64.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/tools/relocs.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/tools/relocs_common.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/tools/relocs.h %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents tools/include/tools/le_byteshift.h %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/purgatory/purgatory.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/purgatory/stack.S %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/purgatory/setup-x86_64.S %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/purgatory/entry64.S %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/boot/string.h %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/boot/string.c %{buildroot}/lib/modules/%{kverstr}/build/
    cp -v -a --parents arch/x86/boot/ctype.h %{buildroot}/lib/modules/%{kverstr}/build/
%endif
    # Make sure the Makefile and version.h have a matching timestamp so that
    # external modules can be built
    touch -r %{buildroot}/lib/modules/%{kverstr}/build/Makefile %{buildroot}/lib/modules/%{kverstr}/build/include/generated/uapi/linux/version.h
 
    # Copy .config to include/config/auto.conf so "make prepare" is unnecessary.
    cp -v %{buildroot}/lib/modules/%{kverstr}/build/.config %{buildroot}/lib/modules/%{kverstr}/build/include/config/auto.conf
 
 
find %{buildroot}/lib/modules/%{kverstr} -name "*.ko" -type f >modnames
 
    # mark modules executable so that strip-to-file can strip them
xargs --no-run-if-empty chmod u+x < modnames
 
    # Generate a list of modules for block and networking.
 
grep -F /drivers/ modnames | xargs --no-run-if-empty nm -upA | sed -n 's,^.*/\([^/]*\.ko\):  *U \(.*\)$,\1 \2,p' > drivers.undef

collect_modules_list() {
	sed -r -n -e "s/^([^ ]+) \\.?($2)\$/\\1/p" drivers.undef |
	LC_ALL=C sort -u > %{buildroot}/lib/modules/%{kverstr}/modules.$1
	if [ ! -z "$3" ]; then
		sed -r -e "/^($3)\$/d" -i %{buildroot}/lib/modules/%{kverstr}/modules.$1
	fi
}

collect_modules_list networking 'register_netdev|ieee80211_register_hw|usbnet_probe|phy_driver_register|rt(l_|2x00)(pci|usb)_probe|register_netdevice'
collect_modules_list block 'ata_scsi_ioctl|scsi_add_host|scsi_add_host_with_dma|blk_alloc_queue|blk_init_queue|register_mtd_blktrans|scsi_esp_register|scsi_register_device_handler|blk_queue_physical_block_size' 'pktcdvd.ko|dm-mod.ko'
collect_modules_list drm 'drm_open|drm_init'
collect_modules_list modesetting 'drm_crtc_init'
 
	# detect missing or incorrect license tags

( find %{buildroot}/lib/modules/%{kverstr} -name '*.ko' | xargs /sbin/modinfo -l | grep -E -v 'GPL( v2)?$|Dual BSD/GPL$|Dual MPL/GPL$|GPL and additional rights$' ) && exit 1
 
pushd %{buildroot}/lib/modules/%{kverstr}/
	rm -f modules.{alias*,builtin.bin,dep*,*map,symbols*,devname,softdep}
popd

mkdir -p %{buildroot}/usr/src/kernels
mv %{buildroot}/lib/modules/%{kverstr}/build %{buildroot}/usr/src/kernels/%{kverstr}

ln -sf /usr/src/kernels/%{kverstr} %{buildroot}/lib/modules/%{kverstr}/build
ln -sf /usr/src/kernels/%{kverstr} %{buildroot}/lib/modules/%{kverstr}/source

find %{buildroot}/usr/src/kernels -name "*.*cmd" -delete
#

cp -v  %{buildroot}/boot/vmlinuz-%{kverstr} %{buildroot}/lib/modules/%{kverstr}/vmlinuz
mkdir -p %{buildroot}/%{_bindir}
cp -v  %{SOURCE2} %{buildroot}/%{_bindir}/rnread-%{name}
chmod u+rwx,go+rx %{buildroot}/%{_bindir}/rnread-%{name}

%clean
rm -rf %{buildroot}

%post core
if [ `uname -i` == "x86_64" -o `uname -i` == "i386" ] &&
   [ -f /etc/sysconfig/kernel ]; then
  /bin/sed -r -i -e 's/^DEFAULTKERNEL=kernel-smp$/DEFAULTKERNEL=kernel/' /etc/sysconfig/kernel || exit $?
fi

%posttrans core
/bin/kernel-install add %{kverstr} /lib/modules/%{kverstr}/vmlinuz || exit $?

### Donation plug
echo -e “CentOS Support by @135e2”
printf "\n\n\n  This package "
printf '%{name}'
printf " is being worked by me as a hobby in my free time\n"
echo -e "  If you're interested in supporting me doing this, you can go to:\n"
echo -e "\t\thttps://liberapay.com/rmnscnce/donate\n\n  And donate so I can keep this package maintained for everyone\n\nYou can read news and announcements for this software using "'rnread-%{name}\n\n\n'

%preun core
/bin/kernel-install remove %{kverstr} /lib/modules/%{kverstr}/vmlinuz || exit $?
if [ -x /usr/sbin/weak-modules ]
then
    /usr/sbin/weak-modules --remove-kernel %{kverstr} || exit $?
fi

%post devel
if [ -f /etc/sysconfig/kernel ]
then
    . /etc/sysconfig/kernel || exit $?
fi
if [ "$HARDLINK" != "no" -a -x /usr/bin/hardlink -a ! -e /run/ostree-booted ] 
then
    (cd /usr/src/kernels/%{kverstr} &&
     /usr/bin/find . -type f | while read f; do
       hardlink -c /usr/src/kernels/*%{?dist}.*/$f $f 2>&1 >/dev/null
     done)
fi

%post modules
/sbin/depmod -a %{kverstr}

%postun modules
/sbin/depmod -a %{kverstr}

%files core
%defattr (-, root, root)
%exclude /%{kverstr}
/boot/*
%exclude /boot/vmlinux-%{kverstr}
/lib/modules/%{kverstr}/vmlinuz
/lib/modules/%{kverstr}/symvers.gz

%files modules
%defattr (-, root, root)
/lib/modules/%{kverstr}
%exclude /lib/modules/%{kverstr}/vmlinuz
%exclude /lib/modules/%{kverstr}/symvers.gz
%exclude /lib/modules/%{kverstr}/build
%exclude /lib/modules/%{kverstr}/source

%files headers
%defattr (-, root, root)
/usr/include

%files devel
%defattr (-, root, root)
/usr/src/kernels/%{kverstr}
/lib/modules/%{kverstr}/build
/lib/modules/%{kverstr}/source

%files
%defattr (-, root, root)
%attr(0755, root, root) %{_bindir}/rnread-%{name}
